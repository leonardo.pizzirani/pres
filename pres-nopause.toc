\babel@toc {italian}{}
\babel@toc {italian}{}
\beamer@sectionintoc {1}{Leggi}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Leggi empiriche}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Legge di Zipf}{4}{0}{1}
\beamer@subsectionintoc {1}{3}{Legge di Heaps}{8}{0}{1}
\beamer@sectionintoc {2}{Bayes}{11}{0}{2}
\beamer@subsectionintoc {2}{1}{Teorema di Bayes}{11}{0}{2}
\beamer@subsectionintoc {2}{2}{Scelta del modello}{13}{0}{2}
\beamer@subsubsectionintoc {2}{2}{1}{Scambiabilità}{16}{0}{2}
\beamer@subsubsectionintoc {2}{2}{2}{Teorema della rappresentazione di De Finetti}{19}{0}{2}
\beamer@subsubsectionintoc {2}{2}{3}{Urna di Pólya}{21}{0}{2}
\beamer@sectionintoc {3}{Dirichlet}{29}{0}{3}
\beamer@subsectionintoc {3}{1}{Processo di Dirichlet}{29}{0}{3}
\beamer@subsectionintoc {3}{2}{Modello di Blackwell MacQueen}{38}{0}{3}
\beamer@sectionintoc {4}{Poisson-Dirichlet}{48}{0}{4}
\beamer@subsectionintoc {4}{1}{Processo di Poisson-Dirichlet}{48}{0}{4}
\beamer@subsectionintoc {4}{2}{Modello del possibile adiacente}{62}{0}{4}
\beamer@sectionintoc {5}{Attribuzione}{72}{0}{5}
\beamer@subsectionintoc {5}{1}{Stima dei parametri continui}{72}{0}{5}
\beamer@subsectionintoc {5}{2}{Stima dei parametri discreti}{74}{0}{5}
\beamer@subsectionintoc {5}{3}{Legge della verosimiglianza}{80}{0}{5}
\beamer@sectionintoc {6}{Testi sintetici}{82}{0}{6}
\beamer@subsectionintoc {6}{1}{Dizionario variabile}{85}{0}{6}
\beamer@subsectionintoc {6}{2}{Processi differenti}{89}{0}{6}
\beamer@subsectionintoc {6}{3}{Parti differenti}{93}{0}{6}
\beamer@sectionintoc {7}{Testi reali}{97}{0}{7}
\beamer@subsectionintoc {7}{1}{Dizionario Italiano}{97}{0}{7}
\beamer@subsectionintoc {7}{2}{Percentuali di attribuzione}{102}{0}{7}
\beamer@subsectionintoc {7}{3}{Algoritmo LZ77}{103}{0}{7}
\beamer@subsectionintoc {7}{4}{Dizionario LZ77}{108}{0}{7}
\beamer@subsectionintoc {7}{5}{Percentuali di attribuzione}{109}{0}{7}
\beamer@sectionintoc {8}{Conclusioni}{110}{0}{8}
\beamer@sectionintoc {9}{Riferimenti}{113}{0}{9}
