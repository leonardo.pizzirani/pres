\babel@toc {italian}{}
\babel@toc {italian}{}
\beamer@subsectionintoc {1}{1}{Introduzione}{2}{0}{0}
\beamer@sectionintoc {2}{Leggi}{3}{0}{1}
\beamer@subsectionintoc {2}{1}{Legge di Zipf}{3}{0}{1}
\beamer@subsectionintoc {2}{2}{Legge di Heaps}{6}{0}{1}
\beamer@sectionintoc {3}{Modello}{8}{0}{2}
\beamer@subsectionintoc {3}{1}{Scambiabilità}{8}{0}{2}
\beamer@subsubsectionintoc {3}{1}{1}{Teorema della rappresentazione di De Finetti}{10}{0}{2}
\beamer@subsectionintoc {3}{2}{Urna di Pólya}{11}{0}{2}
\beamer@sectionintoc {4}{Dirichlet}{13}{0}{3}
\beamer@subsectionintoc {4}{1}{Processo di Dirichlet}{13}{0}{3}
\beamer@subsectionintoc {4}{2}{Modello di Blackwell MacQueen}{16}{0}{3}
\beamer@sectionintoc {5}{Poisson-Dirichlet}{18}{0}{4}
\beamer@subsectionintoc {5}{1}{Processo di Poisson-Dirichlet}{18}{0}{4}
\beamer@subsectionintoc {5}{2}{Modello del possibile adiacente}{26}{0}{4}
\beamer@sectionintoc {6}{Attribuzione}{29}{0}{5}
\beamer@subsectionintoc {6}{1}{Stima dei parametri continui}{29}{0}{5}
\beamer@subsectionintoc {6}{2}{Stima dei parametri discreti}{31}{0}{5}
\beamer@subsectionintoc {6}{3}{Legge della verosimiglianza}{35}{0}{5}
\beamer@sectionintoc {7}{Testi sintetici}{36}{0}{6}
\beamer@subsectionintoc {7}{1}{Stima}{36}{0}{6}
\beamer@subsubsectionintoc {7}{1}{1}{Dizionario variabile}{36}{0}{6}
\beamer@subsectionintoc {7}{2}{Attribuzione}{40}{0}{6}
\beamer@subsubsectionintoc {7}{2}{1}{Processi differenti}{40}{0}{6}
\beamer@subsubsectionintoc {7}{2}{2}{Parti differenti}{43}{0}{6}
\beamer@sectionintoc {8}{Testi reali}{45}{0}{7}
\beamer@subsectionintoc {8}{1}{Dizionario Italiano}{45}{0}{7}
\beamer@subsectionintoc {8}{2}{Percentuali di attribuzione}{47}{0}{7}
\beamer@subsectionintoc {8}{3}{Algoritmo LZ77}{48}{0}{7}
\beamer@subsectionintoc {8}{4}{Dizionario LZ77}{51}{0}{7}
\beamer@subsectionintoc {8}{5}{Percentuali di attribuzione}{52}{0}{7}
\beamer@sectionintoc {9}{Conclusioni}{53}{0}{8}
\beamer@sectionintoc {10}{Riferimenti}{56}{0}{9}
\beamer@sectionintoc {11}{Teorema di Bayes}{62}{0}{10}
\beamer@sectionintoc {12}{Urna di Pólya}{63}{0}{11}
\beamer@sectionintoc {13}{Processo di Dirichlet}{65}{0}{12}
\beamer@sectionintoc {14}{Modello di Blackwell MacQueen}{68}{0}{13}
\beamer@sectionintoc {15}{Modello del possibile adiacente}{72}{0}{14}
