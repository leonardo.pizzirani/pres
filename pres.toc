\babel@toc {italian}{}
\babel@toc {italian}{}
\beamer@sectionintoc {1}{Leggi}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Leggi empiriche}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Legge di Zipf}{3}{0}{1}
\beamer@subsectionintoc {1}{3}{Legge di Heaps}{5}{0}{1}
\beamer@sectionintoc {2}{Modello}{7}{0}{2}
\beamer@subsectionintoc {2}{1}{Scambiabilità}{7}{0}{2}
\beamer@subsubsectionintoc {2}{1}{1}{Teorema della rappresentazione di De Finetti}{9}{0}{2}
\beamer@subsubsectionintoc {2}{1}{2}{Urna di Pólya}{10}{0}{2}
\beamer@sectionintoc {3}{Dirichlet}{13}{0}{3}
\beamer@subsectionintoc {3}{1}{Processo di Dirichlet}{13}{0}{3}
\beamer@subsectionintoc {3}{2}{Modello di Blackwell MacQueen}{16}{0}{3}
\beamer@sectionintoc {4}{Poisson-Dirichlet}{21}{0}{4}
\beamer@subsectionintoc {4}{1}{Processo di Poisson-Dirichlet}{21}{0}{4}
\beamer@subsectionintoc {4}{2}{Modello del possibile adiacente}{30}{0}{4}
\beamer@sectionintoc {5}{Attribuzione}{35}{0}{5}
\beamer@subsectionintoc {5}{1}{Stima dei parametri continui}{35}{0}{5}
\beamer@subsectionintoc {5}{2}{Stima dei parametri discreti}{37}{0}{5}
\beamer@subsectionintoc {5}{3}{Legge della verosimiglianza}{41}{0}{5}
\beamer@sectionintoc {6}{Testi sintetici}{42}{0}{6}
\beamer@subsectionintoc {6}{1}{Dizionario variabile}{43}{0}{6}
\beamer@subsectionintoc {6}{2}{Processi differenti}{47}{0}{6}
\beamer@subsectionintoc {6}{3}{Parti differenti}{50}{0}{6}
\beamer@sectionintoc {7}{Testi reali}{52}{0}{7}
\beamer@subsectionintoc {7}{1}{Dizionario Italiano}{52}{0}{7}
\beamer@subsectionintoc {7}{2}{Percentuali di attribuzione}{54}{0}{7}
\beamer@subsectionintoc {7}{3}{Algoritmo LZ77}{55}{0}{7}
\beamer@subsectionintoc {7}{4}{Dizionario LZ77}{58}{0}{7}
\beamer@subsectionintoc {7}{5}{Percentuali di attribuzione}{59}{0}{7}
\beamer@sectionintoc {8}{Conclusioni}{60}{0}{8}
\beamer@sectionintoc {9}{Riferimenti}{61}{0}{9}
